var shopping_cart = [];
var cartname;
$(document).ready(function() {
	
	console.log("GETTING USER");
	$.ajax({
		url : 'http://localhost:8080/bookstore/api/user/current',
		type : 'get',
		dataType : 'json',
		contentType : "application/json; charset=utf-8",

		success : function(data) {
			console.log("!!")
			console.log("SUCCESS " + data);
			cartname = "cart_" + data["id"];
			console.log(cartname);
			initializeCart();
		}, 
		error : function(data) {
			console.log("ERROR " + data);
		}
	});

	
});

function initializeCart() {
	var cart = localStorage.getItem(cartname);

	if (typeof cart == 'undefined' || cart == null) {
		var cart = [];
		localStorage.setItem(cartname, JSON.stringify(cart));

	} else {
		console.log("CART IS DEFINED");
	}

}

function addToCart(book_id) {
	getBook(book_id);
}

function addItemToSession(book) {
	console.log("ADDING ITEM");
	//var newBook = JSON.stringify(data);
	//var bookObject = data;
	//console.log("NEW BOOK " + bookObject["stockLevel"])
	
	if(book["numberLeftInStock"] > 0) {

		console.log("NEW BOOK " + newBook);
		var newBook = JSON.stringify(book);
		var shopCart = JSON.parse(localStorage.getItem(cartname));

		shopCart.push(newBook);
		console.log("ADDING ITEM");
		console.log(shopCart);
		localStorage.setItem(cartname, JSON.stringify(shopCart));
	} else {
		alert("Out of stock!")
	}

}

function getBook(book_id) {

	$.ajax({
		url : 'http://localhost:8080/bookstore/api/book/' + book_id,
		type : 'get',
		contentType : "application/json; charset=utf-8",
		success : function(data) {
			console.log(data);
			var book = new BookOrder(data["id"], data["name"], data["author"]["id"], data["author"]["name"], null, data["stockLevel"], data["price"]);
			console.log("BOOK " + book)
			addItemToSession(book);
			
		},
	});

}