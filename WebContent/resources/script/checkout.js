var cartname;
var currentUser;

$(document).ready(function() {

	setUpCart();

});

function setUpCart() {

	getCurrentUser();
};

function getCurrentUser() {
	console.log("GETTING USER");
	$.ajax({
		url : 'http://localhost:8080/bookstore/api/user/current',
		type : 'get',
		dataType : 'json',
		contentType : "application/json; charset=utf-8",

		success : function(data) {
			console.log("!!")
			console.log("SUCCESS " + data);
			cartname = "cart_" + data["id"];
			console.log(cartname);
			currentUser = data;
			initializeCart();
		},
		error : function(data) {
			console.log("ERROR " + data);
		}
	});
}

function calculatePrice(shopping_cart) {
	
	var price = 0;
	
	for(item in shopping_cart) {
		
		var book = JSON.parse(shopping_cart[item]);
		price = price + book["price"];
	}
	
	 $('#price').html(price);
	
	
}

function initializeCart() {

	var cart = localStorage.getItem(cartname);

	if (typeof cart != 'undefined' || cart != null) {

		var shopCart = JSON.parse(cart);

		createCart(shopCart);

	} else {

		var cart = [];

		localStorage.setItem(cartname, JSON.stringify(cart));
	}

}

function createCart(shopCart) {

	var shopping_cart = [];

	for (item in shopCart) {

		var book = shopCart[item];
		shopping_cart.push(book);

	}

	displayCart(shopping_cart);
	calculatePrice(shopping_cart);
}

function displayCart(shopping_cart) {

	if (shopping_cart.length == 0) {

		//Cart is empty
		
		var row_data = "Basket is empty";

		var start_row = '<tr><td colspan="4">';
		var end_row = '</td></tr>';
		var new_row = start_row + row_data + end_row;

		$('#basket tr:first').after(new_row);

	} else {
		
		for (x in shopping_cart) {

			//populate the table with the items in the cart
			
			console.log(shopping_cart[x]);
			var book = JSON.parse(shopping_cart[x]);
			
			var new_row = tableRow.addRow(book);
			console.log("NEW ROW " + new_row);

			$('#basket tr:first').after(new_row);

		}
	}
}

var tableRow = (function() {

	var book;

	function createNewRow(book) {

		this.book = book;
		
		var start_row = '<tr>';
		var end_row = '</tr>';
		var book_name = '<td>' + book.bookName + '</td>';

		var author_name = '<td>' + book.authorName + '</td>';

		var stock_level = '<td>' + book.numberLeftInStock + '</td>';

		var price = '<td>' + book.price + '</td>';

		var remove_button = '<td><button onclick="removeFromCart('
				+ book.bookId + ')" class="btn-default">X</button></td>';

		var new_row = start_row + book_name + author_name + stock_level + price
				+ remove_button + end_row;

		return new_row;

	}

	return {

		addRow : function(book) {
			return createNewRow(book);
		},

	};

}());

function removeFromCart(id) {
	console.log("ID " + id);
	var shopCart = JSON.parse(localStorage.getItem(cartname));

	console.log(shopCart);
	
	for (var i = 0; i < shopCart.length; i++) {
		
		var item = JSON.parse(shopCart[i]);

		if (item.bookId === id) {
			shopCart.splice(i, 1);
			break;
		}

	}

	var newCart = [];

	for (z in shopCart) {
		newCart.push(shopCart[z]);
	}

	console.log(newCart);
	var cartStr = JSON.stringify(newCart);

	localStorage.setItem(cartname, cartStr);

	location.reload();
}

function createOrder() {
	var cart = localStorage.getItem(cartname);
	
	if (cart.length > 0) {

		cart = JSON.parse(cart);

		var totalPrice = 0;
		for (item in cart) {
			var book = cart[item];
			var parsedBook = JSON.parse(book);
			cart[item] = parsedBook;
			totalPrice = totalPrice + parsedBook["price"];
		}

		var shopCart = new ShoppingCart();
		shopCart.bookOrders = cart;
		cart.totalPrice = totalPrice;

		var paymentType = null;

		if (currentUser["paymentMethod"] == "Paypal") {
			paymentType = new Paypal();
		} else if (currentUser["paymentMethod"] == "Visa") {
			paymentType = new Visa();
		} else {
			paymentType = new OtherPayment();
		}
		
		shopCart.setPaymentStrategy(paymentType);
		shopCart.calculate(cart);
		var shoppingCartString = JSON.stringify(shopCart);

		$.ajax({
					url : 'http://localhost:8080/bookstore/api/order/create',
					type : 'post',
					dataType : 'json',
					contentType : "application/json; charset=utf-8",

					success : function(data) {
						console.log("SUCCESS");

						var cart = [];

						localStorage.setItem(cartname, JSON.stringify(cart));

						window.location.href = "http://localhost:8080/bookstore/orders";
					},

					data : shoppingCartString
				});
	}

}

function clearCart() {

	var cart = [];

	localStorage.setItem(cartname, JSON.stringify(cart));

	location.reload();

}
