function ShoppingCart (bookOrders, totaPrice) {
	this.bookOrders = bookOrders;
	this.totaPrice = totaPrice;
	this.paymentCharge = 0;
	this.paymentType = "";
};

ShoppingCart.prototype = {
		setPaymentStrategy: function(paymentType) {
			this.paymentType = paymentType;
		},
		
		calculate: function(cart) {
			this.paymentCharge = this.paymentType.calculate(cart);
		}
};

function BookOrder(bookId, bookName, authorId, authorName, quantity, numberLeftInStock, price, paymentCharge) {
	this.bookId = bookId,
	this.bookName = bookName,
	this.authorId = authorId,
	this.authorName = authorName,
	this.quantity = quantity,
	this.numberLeftInStock = numberLeftInStock
	this.price = price;
	this.paymentCharge = paymentCharge;
};


var Paypal = function() {
	this.calculate = function(cart) {
		
		/*
		 * Paypal charge 1.5 per item 
		 */
		
		var numItems = cart.length;
		var chargePerItem = 1.5;
		
		return numItems * chargePerItem;
	}
};

var Visa = function() {
	this.calculate = function(cart) {
		
		/*
		 * Visa charge 5% of total price
		 */
		
		var charge = cart.totalPrice * 0.05;
		charge = Math.round(charge * 100) / 100
		return charge;
	}
};

var OtherPayment = function() {
	this.calculate = function(cart) {
		return 0;
	}
};