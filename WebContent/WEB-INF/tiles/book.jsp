<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<div class="container">
	<h1>${book.name }</h1>
	<div class="col-md-4">
		<img src="<c:url value="${book.bookCoverUrl}"/>" height="392" width="250">
	</div>
	<div class="col-md-8">
		<table class="table">
			<tr>
				<th colspan="2">Book Details</th>
			</tr>
			<tr>
				<th>Price</th>
				<td>${book.price }</td>
			</tr>
			<tr>
				<th>Year Released</th>
				<td>${book.yearReleased }</td>
			</tr>
			<tr>
				<th>Author</th>
				<td><a href="<c:url value="/authors/${book.author.id}"/>">${book.author.name}</a></td>
			</tr>
			<tr>
				<th>ISBN</th>
				<td>${book.isbn }</td>
			</tr>
			<tr>
				<th>No. Pages</th>
				<td>${book.numberOfPages}</td>
			</tr>
			<tr>
				<th>No. Left in Stock</th>
				<td>${book.stockLevel}</td>
			</tr>
			<tr>
				<td><sec:authorize access="!hasRole('ROLE_ADMIN')">
						<td><button class="btn-default" onclick="addToCart(${book.id})">Add To Cart</button></td>
					</sec:authorize></td>
			</tr>
		</table>
	</div>
</div>
