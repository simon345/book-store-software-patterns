<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container">
<h1>Authors</h1>
	<table class="table table-striped">
		<tr>
			<th>Name</th>
		</tr>
		<c:forEach var="author" items="${authors}">
			<tr>
				<td><a href="<c:url value="/authors/${author.id}"/>">${author.name}</a></td>
			</tr>
		</c:forEach>
	</table>
</div>