<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="container">
	<h1>${author.name }</h1>
	<h3>Books</h3>
	<table class="table">
		<tr>
			<th>Book Title</th>
			<th>Price</th>
			<th>No. Left in Stock</th>
		</tr>
		<c:forEach var="book" items="${author.books}">
			<tr>
				<td><a href="<c:url value="/books/${book.id}"/>">${book.name}</a></td>
				<td>${book.price }</td>
				<td>${book.stockLevel }</td>
			</tr>
		</c:forEach>

	</table>
</div>