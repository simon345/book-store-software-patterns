<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<div class="container">

	<div class="wrapper">
		<sf:form class="form" method="post" id="createForm" action="${pageContext.request.contextPath}/createaccount" commandName="user">

			<h3 class="form-signin-heading">Create Your Free Account</h3>
			<div class="form-group">
				<label for="usernameInput">Username</label>
				<sf:input class="form-control" id="usernameInput" path="username" name="username" type="text" />
				<sf:errors path="username" cssClass="error"></sf:errors>
			</div>
			<div class="form-group">
				<label for="nameInput">Name</label>
				<sf:input class="form-control" id="nameInput" path="name" name="name" type="text" />
			</div>
			<div class="form-group">
				<label for="paymentMethodInput">Payment Method</label>
				<sf:select id="paymentMethodInput" path="paymentMethod" name="paymentMethod" >
				<sf:option value="Paypal" />
				<sf:option value="Visa" />
				</sf:select>
			</div>
			<div class="form-group">
				<label for="shippingAddressInput">Shipping Address</label>
				<sf:input class="form-control" id="shippingAddressInput" path="shippingAddress" name="shippingAddress" type="text" />
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<sf:input class="form-control" id="password" path="password" name="password" type="password" />
				<sf:errors path="password" cssClass="error"></sf:errors>
			</div>
			<div class="form-group">
				<label for="confirmPassword">Confirm Password</label> <input class="form-control" id="confirmPassword" name="confirmpass" type="password" />
			</div>
			<input class="btn-default btn-block" value="Create Account" type="submit" />
		</sf:form>		
	</div>
</div>