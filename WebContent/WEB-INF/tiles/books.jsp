<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<div class="container">
	<h1>Books</h1>
	<div>
		<form class="form-inline" method="post" id="searchBook" action="${pageContext.request.contextPath}/dosearch">
			<div class="form-group">
				<input placeholder="Search for book" class="form-control" id="nameInput" name="name" type="text" /> <select name="searchSelection">
					<option value="title">Book Title</option>
					<option value="author">Author</option>
				</select> <input class="btn-default" value="Search" type="submit" /> <a class="btn-default" href="${pageContext.request.contextPath}/books">View All</a>
			</div>
		</form>
	</div>
	<table class="table table-striped sortable">
		<tr>
			<th></th>
			<th>Name</th>
			<th>Author</th>
			<th>Price</th>
			<th>Stock Level</th>
			<sec:authorize access="!hasRole('ROLE_ADMIN')">
				<th></th>
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<th>Edit</th>
			</sec:authorize>
		</tr>
		<c:forEach var="book" items="${books}">
			<tr>
				<td><img src="<c:url value="${book.bookCoverUrl}"/>" height="60" width="30"></td>
				<td><a href="<c:url value="/books/${book.id}"/>">${book.name}</a></td>
				<td><a href="<c:url value="/authors/${book.author.id}"/>">${book.author.name}</a></td>
				<td>${book.price}</td>
				<td id="stockLevel">${book.stockLevel}</td>
				<sec:authorize access="!hasRole('ROLE_ADMIN')">
					<td><button class="btn-default" onclick="addToCart(${book.id})">Add To Cart</button></td>
				</sec:authorize>
				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<td><a href="<c:url value="/book/edit/${book.id}"/>">Edit</a></td>
				</sec:authorize>
			</tr>
		</c:forEach>
	</table>
</div>