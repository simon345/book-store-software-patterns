<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="container">
	<h1>${user.username }</h1>

	<h1>${user.username }'s Orders</h1>
	
	<c:if test="${fn:length(user.orders) == 0}">
	User has no orders
	</c:if>

	<c:if test="${fn:length(user.orders) > 0}">

		<c:forEach var="order" items="${user.orders}">
			<table class="table">
				<tr>
					<th colspan="4" align="right">${order.orderDate}</th>
				</tr>
				<tr>
					<th>Book</th>
					<th>Author</th>
					<th>Price</th>
					<th>Quantity</th>
				</tr>
				<c:forEach var="item" items="${order.orderItems}">
					<tr>
						<td><a href="<c:url value="/books/${item.book.id}"/>">${item.book.name}</a></td>
						<td><a href="<c:url value="/authors/${item.book.author.id}"/>">${item.book.author.name}</a></td>
						<td>${item.book.price}</td>
						<td>${item.quantity}</td>
					</tr>
				</c:forEach>
				<tr>
					<td colspan="4" class="active" align="right">Total Price ${order.totalPrice }</td>
				</tr>
				<c:if test="${order.discountApplied}">
					<tr>
						<td colspan="4" class="active" align="right">Discount ${order.discount }</td>
					</tr>
					<tr>
						<td colspan="4" class="active" align="right">Price After Discount ${order.priceAfterDiscount }</td>
					</tr>
				</c:if>
			</table>
		</c:forEach>
	</c:if>
</div>