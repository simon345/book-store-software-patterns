<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container">
	<div class="wrapper">
		<form class="user-form-vertical" name='f' action='${pageContext.request.contextPath}/login' method='POST'>
			<h3>Sign in Below</h3>
			<c:if test="${param.error != null }">
				<span class="login_error">User credentials not correct</span>
			</c:if>
			<div class="form-group">
				<label for="username">Username</label> <input class="form-control" type='text' id="username" name='username' value=''>
			</div>
			<div class="form-group">
				<label for="password">Password</label> <input class="form-control" type='password' name='password' id="password" />
			</div>

			<div class="checkbox">
				<label><input type='checkbox' name='remember-me' checked="checked" />Remember Me:</label>
			</div>

			<input class="defaultbtn btn-block" name="submit" type="submit" value="SIGN IN" /> <input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
	</div>
</div>