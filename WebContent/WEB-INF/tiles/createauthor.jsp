<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<sf:form method="post" action="${pageContext.request.contextPath}/docreateauthor" commandName="author">

	<table class="formtable">
		<tr>
			<td class="label">Name:</td>
			<td><sf:input class="control" path="name" name="name" type="text" /><br />
			<sf:errors path="name" cssClass="error"></sf:errors></td>
			<td class="label"> </td><td><input class="control"  value="Create Author" type="submit" /></td>		
		</tr>
	</table>

</sf:form>