<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container">
	<h1>Create a New Book</h1>
	<sf:form method="post" action="${pageContext.request.contextPath}/docreatebook" commandName="book">
		<table class="formtable">
			<tr>
				<td>Name:</td>
				<td><sf:input class="control" path="name" name="name" type="text" /><br /> <sf:errors path="name" cssClass="error"></sf:errors></td>
			</tr>
			<tr>
				<td>Author:</td>
				<td><sf:select path="author">
						<c:forEach var="author" items="${authors}">
							<sf:option value="${author.name}" label="${author.name}" />
						</c:forEach>
					</sf:select></td>
			</tr>
			<tr>
				<td>Stock Level</td>
				<td><sf:input class="control" path="stockLevel" name="stockLevel" type="number" /><br /></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><sf:input class="control" path="price" name="price" type="number" /><br /></td>
			</tr>
			<tr>
				<td>Book Cover Url</td>
				<td><sf:input class="control" path="bookCoverUrl" name="bookCoverUrl" type="text" /><br /></td>
			</tr>			
			<tr>
				<td>ISBN</td>
				<td><sf:input class="control" path="isbn" name="isbn" type="text" /><br /></td>
			</tr>
			<tr>
				<td>No. Pages</td>
				<td><sf:input class="control" path="numberOfPages" name="numberOfPages" type="number" /><br /></td>
			</tr>
			<tr>
				<td>Year Released</td>
				<td><sf:input class="control" path="yearReleased" name="yearReleased" type="number" /><br /></td>
			</tr>
			<tr>
				<td></td>
				<td><input class="control" value="Create Book" type="submit" /></td>
			</tr>
		</table>
	</sf:form>
</div>