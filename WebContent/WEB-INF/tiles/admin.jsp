<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
Admin page

<table class="table">
	<tr>
		<td>Username</td>
		<td>Name</td>
		<td>Role</td>
		<td>Enabled</td>
	</tr>
	<c:forEach var="user" items="${users}">
		<tr>
			<td><a href="<c:url value="/user/${user.id}"/>">${user.username}</a></td>
			<td><c:out value="${user.name }"></c:out></td>
			<td><c:out value="${user.authority }"></c:out></td>
			<td><c:out value="${user.enabled }"></c:out></td>
	</c:forEach>

</table>