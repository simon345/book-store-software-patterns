<div class="container">
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<h1>Checkout</h1>
	<table id="basket" class="table">
		<tr>
			<th>Name</th>
			<th>Author</th>
			<th>No. Left in Stock</th>
			<th>Price</th>
			<th></th>
		</tr>
		<c:if test="${user.firstPurchase}">
			<tr>
				<td colspan="5">This is your first purchase, you get 15% off!</td>
			</tr>
		</c:if>
		<tr>
			<td></td>
			<td></td>
			<td><b>Price</b></td>
			<td id="price" align="left"></td>
			<td></td>
		</tr>
		<tr class="table-active">
			<td></td>
			<td></td>
			<td><button onclick="clearCart()" class="btn-default">Clear Basket</button></td>
			<td><button onclick="createOrder()" class="btn-default">Buy</button></td>
			<td></td>
		</tr>
	</table>
</div>