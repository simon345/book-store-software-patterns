<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<nav class="navbar navbar-default navbar-static-top">
	<div class="container">
		<ul class="nav navbar-nav">
			<li><a class="navbar-brand" href="<c:url value="/"/>">Book Store</a></li>
			<li><a href="<c:url value="/books"/>">Books</a></li>
			<li><a href="<c:url value="/authors"/>">Authors</a></li>

			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<li><a href="<c:url value="/admin"/>">ADMIN</a></li>
				<li><a href="<c:url value="/createbook"/>">New Book</a></li>
				<li><a href="<c:url value="/createauthor"/>">New Author</a></li>
			</sec:authorize>
			<sec:authorize access="!isAuthenticated()">
				<li><a href="${pageContext.request.contextPath}/login">LOGIN</a></li>
				<li><a href="${pageContext.request.contextPath}/newaccount">REGISTER</a></li>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
			
			<li><a href="<c:url value="/checkout"/>">Checkout</a></li>
			<li><a href="<c:url value="/orders"/>">Your Orders</a></li>
				<sec:authentication property="principal.username" var="username" />
				<li class="navbar-right"><c:url value="/logout" var="logoutUrl" /></li>
				<li class="navbar-right"><a href="${logoutUrl}">LOG OUT</a></li>
				<li class="navbar-right">${username}</li>
			</sec:authorize>
		</ul>
	</div>
</nav>