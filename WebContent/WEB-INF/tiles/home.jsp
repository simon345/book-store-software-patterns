<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container">
	<h1>Home Page</h1>

	<div class="jumbotron">
		<h3><a href="<c:url value="/books"/>">View All Books</a></h3>
	</div>	
	<div class="jumbotron">
		<h3><a href="<c:url value="/authors"/>">View All Authors</a></h3>
	</div>	
	<div class="jumbotron">
		<h3><a href="<c:url value="/checkout"/>">Checkout</a></h3>
	</div>	
	<div class="jumbotron">
		<h3><a href="<c:url value="/orders"/>">View All Your Orders</a></h3>
	</div>
</div>