package com.bookstore.web.entities;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Discount implements IDiscount {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private double discountTotal;

	private double discountPercentage;

	private double priceAfterDiscount;

	private DiscountType type;

	@OneToOne
	private CustomerOrder customerOrder;

	public Discount() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(double discountTotal) {
		this.discountTotal = discountTotal;
	}

	public CustomerOrder getCustomerOrder() {
		return customerOrder;
	}

	public void setCustomerOrder(CustomerOrder customerOrder) {
		this.customerOrder = customerOrder;
	}

	public DiscountType getType() {
		return type;
	}

	public void setType(DiscountType type) {
		this.type = type;
	}

	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public double getPriceAfterDiscount() {
		return priceAfterDiscount;
	}

	public void setPriceAfterDiscount(double priceAfterDiscount) {
		this.priceAfterDiscount = priceAfterDiscount;
	}

	@Override
	public void applyDiscount() {

		this.discountPercentage = 0.15;
		this.discountTotal = customerOrder.getTotalPrice() * discountPercentage;

		BigDecimal bd = new BigDecimal(discountTotal);
		bd = bd.setScale(2, RoundingMode.HALF_UP);

		this.discountTotal = bd.doubleValue();

		this.priceAfterDiscount = customerOrder.getTotalPrice() - discountTotal;
	}

}
