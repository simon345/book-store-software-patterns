package com.bookstore.web.entities;

public class BookOrder {

	private int bookId;
	private String bookName;
	private int authorId;
	private String authorName;
	private int quantity;
	private int numberLeftInStock;
	private double price;
	
	public BookOrder() {	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getNumberLeftInStock() {
		return numberLeftInStock;
	}

	public void setNumberLeftInStock(int numberLeftInStock) {
		this.numberLeftInStock = numberLeftInStock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
