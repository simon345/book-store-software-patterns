package com.bookstore.web.entities;

import java.util.List;

public class ShoppingCart {
	
	private List<BookOrder> bookOrders;
	private double totalPrice;
	private double paymentCharge;
	
	public ShoppingCart() {	}

	public List<BookOrder> getBookOrders() {
		return bookOrders;
	}

	public void setBookOrders(List<BookOrder> bookOrders) {
		this.bookOrders = bookOrders;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public double getPaymentCharge() {
		return paymentCharge;
	}

	public void setPaymentCharge(double paymentCharge) {
		this.paymentCharge = paymentCharge;
	}
	

}
