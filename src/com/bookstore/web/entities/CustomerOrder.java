package com.bookstore.web.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
public class CustomerOrder implements IDiscountDecorator{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private User user;

	@OneToMany(mappedBy="order", targetEntity=OrderItem.class)
	@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
	List<OrderItem> orderItems;
	
	@OneToOne(mappedBy = "customerOrder", targetEntity=Discount.class)
	@JsonIgnore
	private Discount discount;
	
	private Date orderDate;
	
	private double totalPriceOfItems;
		
	private double paymentCharge;
	
	private double totalPrice;
	
	private boolean discountApplied;
	
	public CustomerOrder() {	}
	
	public void addItemToOrder(OrderItem item) {
		
		if(orderItems == null) {
			orderItems = new ArrayList<OrderItem>();
		}
		
		orderItems.add(item);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}
	
	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public double getTotalPriceOfItems() {
		return totalPriceOfItems;
	}

	public void setTotalPriceOfItems(double totalPriceOfItems) {
		this.totalPriceOfItems = totalPriceOfItems;
	}

	public double getPaymentCharge() {
		return paymentCharge;
	}

	public void setPaymentCharge(double paymentCharge) {
		this.paymentCharge = paymentCharge;
	}

	public boolean isDiscountApplied() {
		return discountApplied;
	}

	public void setDiscountApplied(boolean discountApplied) {
		this.discountApplied = discountApplied;
	}



	public Discount getDiscount() {
		return discount;
	}

	public void setDiscount(Discount discount) {
		this.discount = discount;
	}

		@Override
		public void applyDiscount(IDiscount discount) {
	
			discount.applyDiscount();
			setDiscountApplied(true);
			
		}


	
}

