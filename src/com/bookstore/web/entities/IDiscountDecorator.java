package com.bookstore.web.entities;

public interface IDiscountDecorator {

	void applyDiscount(IDiscount discount);
	
}
