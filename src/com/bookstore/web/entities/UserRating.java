package com.bookstore.web.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class UserRating {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;	
	
	@ManyToOne(fetch=FetchType.EAGER)
	private User user;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Book book;
	
	private int rating;	
	
	public UserRating() {	}
	
	
	
}
