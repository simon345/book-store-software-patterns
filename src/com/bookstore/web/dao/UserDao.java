package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.web.dao.interfaces.IUserDao;
import com.bookstore.web.entities.Book;
import com.bookstore.web.entities.User;

@Transactional
@Component("userdao")
public class UserDao extends ObjectDaoImpl<User>implements IUserDao {

	private PasswordEncoder passwordEncoder;

	@Autowired
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public void persist(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		super.persist(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAll() {
		return session().createQuery("from User").list();
	}

	@Override
	public User findById(int id) {
		Criteria crit = session().createCriteria(User.class);
		crit.add(Restrictions.eq("id", id));

		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) crit.list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}

	@Override
	public User findByUsername(String username) {
		Criteria crit = session().createCriteria(User.class);
		crit.add(Restrictions.eq("username", username));

		@SuppressWarnings("unchecked")
		List<User> users = (List<User>) crit.list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}
}
