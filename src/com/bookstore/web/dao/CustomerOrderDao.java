package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.web.dao.interfaces.ICustomerOrderDao;
import com.bookstore.web.entities.Book;
import com.bookstore.web.entities.CustomerOrder;
import com.bookstore.web.entities.OrderItem;
import com.bookstore.web.entities.User;

@Transactional
@Component("customerOrderDao")
public class CustomerOrderDao extends ObjectDaoImpl<CustomerOrder>implements ICustomerOrderDao {

	@Override
	public List<CustomerOrder> getAll() {
		Query query = session().createQuery("Select o from CustomerOrder o ORDER BY b.orderDate DESC");

		@SuppressWarnings("unchecked")
		List<CustomerOrder> orders = query.list();

		return orders;
	}

	@Override
	public CustomerOrder findById(int id) {
		Criteria crit = session().createCriteria(CustomerOrder.class);
		crit.add(Restrictions.eq("id", id));

		@SuppressWarnings("unchecked")
		List<CustomerOrder> orders = (List<CustomerOrder>) crit.list();

		if (orders.size() > 0) {
			return orders.get(0);
		} else {
			return null;
		}
	}

	@Override
	public CustomerOrder findUsersLatestOrder(User user) {

		Criteria crit = session().createCriteria(CustomerOrder.class);
		crit.add(Restrictions.eq("user", user));
		crit.addOrder(Order.desc("orderDate"));

		@SuppressWarnings("unchecked")
		List<CustomerOrder> orders = (List<CustomerOrder>) crit.list();

		if (orders.size() > 0) {
			return orders.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void persist(OrderItem item) {
		session().save(item);
	}

	@Override
	public List<OrderItem> findOrderItemsByOrderId(CustomerOrder order) {
		Criteria crit = session().createCriteria(OrderItem.class);
		crit.add(Restrictions.eq("order", order));

		@SuppressWarnings("unchecked")
		List<OrderItem> items = (List<OrderItem>) crit.list();

		return items;

	}

	@Override
	public List<CustomerOrder> findAllUsersOrders(User user) {
		Criteria crit = session().createCriteria(CustomerOrder.class);
		crit.add(Restrictions.eq("user", user));
		crit.addOrder(Order.asc("orderDate"));

		@SuppressWarnings("unchecked")
		List<CustomerOrder> orders = (List<CustomerOrder>) crit.list();

		System.out.println("RETURNING " + orders.size() + " orders");
		
		return orders;
	}

}
