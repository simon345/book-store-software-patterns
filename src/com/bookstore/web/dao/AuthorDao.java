package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.web.dao.interfaces.IAuthorDao;
import com.bookstore.web.dao.interfaces.ISearchCommandDao;
import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;

@Transactional
@Component("authorDao")
public class AuthorDao extends ObjectDaoImpl<Author> implements IAuthorDao{
	
	public Author getAuthorByName(String authorName) {
		
		Query query =  session().createQuery("SELECT a from Author a WHERE a.name like ? ORDER BY a.name ASC");
		query.setParameter(0, authorName);
		
		@SuppressWarnings("unchecked")
		List<Author> authors = query.list();
		
		if(authors.size() > 0) {
			return authors.get(0);
		} else {
			return null;
		}
		
	}

	
	public Author getAuthorByBook(Book b) {
		// TODO Auto-generated method stub
		return null;
	}

	public void create(Author author) {
		session().save(author);
	}


	@Override
	public void delete(Author t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Author> getAll() {
		Query query =  session().createQuery("SELECT a from Author a ORDER BY a.name ASC");

		@SuppressWarnings("unchecked")
		List<Author> authors = query.list();
		
		return authors;
	}

	@Override
	public Author findById(int id) {
		Criteria crit = session().createCriteria(Author.class);
		crit.add(Restrictions.eq("id", id));
	
		@SuppressWarnings("unchecked")
		List<Author> authors = (List<Author>) crit.list();
	
		if (authors.size() > 0) {
			return authors.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<Author> findBySearchTerm(String searchTerm) {
		
		Query query =  session().createQuery("SELECT a from Author a WHERE a.name like ? ORDER BY a.name ASC");
		query.setParameter(0, "%" + searchTerm + "%");
		
		@SuppressWarnings("unchecked")
		List<Author> authors = query.list();
		
		return authors;
	}

	
}
