package com.bookstore.web.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.web.dao.interfaces.ObjectDao;
import com.bookstore.web.entities.Book;
import com.bookstore.web.entities.CustomerOrder;

@Transactional
@Component("objectDao")
public abstract class ObjectDaoImpl<T> implements ObjectDao<T> {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void persist(Object t) {
		session().save(t);
	}

	@Override
	public void delete(Object object) {
		// TODO Auto-generated method stub

	}

	@Override
	public void merge(T t) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(t);
		transaction.commit();
		session.flush();
		session.clear();
		session.close();
	}
}
