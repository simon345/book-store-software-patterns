package com.bookstore.web.dao;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.web.dao.interfaces.IDiscountDao;
import com.bookstore.web.dao.interfaces.ObjectDao;
import com.bookstore.web.entities.CustomerOrder;
import com.bookstore.web.entities.Discount;

@Transactional
@Component("discountDao")
public class DiscountDao extends ObjectDaoImpl<Discount> implements IDiscountDao {

	@Override
	public List<Discount> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Discount findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Discount findDiscountByOrderId(CustomerOrder order) {
		// TODO Auto-generated method stub
		return null;
	}

}
