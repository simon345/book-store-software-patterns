package com.bookstore.web.dao.interfaces;

import com.bookstore.web.entities.User;

public interface IUserDao extends ObjectDao<User>{

	User findByUsername(String username);
	

}
