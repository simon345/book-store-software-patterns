package com.bookstore.web.dao.interfaces;

import java.util.List;

public interface ObjectDao<T> {

	void persist(T t);
	
	void delete(T t);
	
	void merge(T t);
	
	List<T> getAll();
	
	T findById(int id);

}