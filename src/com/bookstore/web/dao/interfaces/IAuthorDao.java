package com.bookstore.web.dao.interfaces;

import java.util.List;

import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;

public interface IAuthorDao extends ObjectDao<Author>, ISearchCommandDao<Author> {
	
	public Author getAuthorByName(String authorName);
	
	public Author getAuthorByBook(Book b);
}
