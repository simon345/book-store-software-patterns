package com.bookstore.web.dao.interfaces;

import java.util.List;

import com.bookstore.web.entities.CustomerOrder;
import com.bookstore.web.entities.OrderItem;
import com.bookstore.web.entities.User;

public interface ICustomerOrderDao extends ObjectDao<CustomerOrder> {

	CustomerOrder findUsersLatestOrder(User user);

	void persist(OrderItem item);

	List<OrderItem> findOrderItemsByOrderId(CustomerOrder order);

	List<CustomerOrder> findAllUsersOrders(User user);

}
