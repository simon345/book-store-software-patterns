package com.bookstore.web.dao.interfaces;

import java.util.List;

public interface ISearchCommandDao<T> {

	List<T> findBySearchTerm(String searchTerm);
	
}
