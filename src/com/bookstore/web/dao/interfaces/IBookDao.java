 package com.bookstore.web.dao.interfaces;

import java.util.List;

import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;

public interface IBookDao extends ObjectDao<Book>, ISearchCommandDao<Book> {

	public List<Book> findBooksByAuthors(List<Author> authors);
	
}
