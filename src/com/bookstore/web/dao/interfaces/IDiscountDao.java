package com.bookstore.web.dao.interfaces;

import com.bookstore.web.entities.CustomerOrder;
import com.bookstore.web.entities.Discount;

public interface IDiscountDao extends ObjectDao<Discount>{

	Discount findDiscountByOrderId(CustomerOrder order);
	
}
