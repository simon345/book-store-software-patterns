package com.bookstore.web.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.web.dao.interfaces.IBookDao;
import com.bookstore.web.dao.interfaces.ISearchCommandDao;
import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;

@Transactional
@Component("bookDao")

public class BookDao extends ObjectDaoImpl<Book>implements IBookDao, ISearchCommandDao<Book> {

	public List<Book> findBooksByAuthors(List<Author> authors) {

		List<Book> books = new ArrayList<Book>();

		for (Author author : authors) {
			Criteria crit = session().createCriteria(Book.class);
			crit.add(Restrictions.eq("author", author));
			List<Book> authorsBooks = crit.list();
			books.addAll(authorsBooks);
		}

		return books;
	}

	@Override
	public List<Book> getAll() {
		Query query = session().createQuery("Select b from Book b ORDER BY b.name ASC");

		@SuppressWarnings("unchecked")
		List<Book> books = query.list();

		return books;
	}

	@Override
	public Book findById(int id) {
		Criteria crit = session().createCriteria(Book.class);
		crit.add(Restrictions.eq("id", id));

		@SuppressWarnings("unchecked")
		List<Book> books = (List<Book>) crit.list();

		if (books.size() > 0) {
			return books.get(0);
		} else {
			return null;
		}
	}

	@Override
	public void persist(Book t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Book t) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Book> findBySearchTerm(String searchTerm) {
		Query query = session().createQuery("SELECT b from Book b WHERE b.name like ? ORDER BY b.name ASC");
		query.setParameter(0, "%" + searchTerm + "%");

		@SuppressWarnings("unchecked")
		List<Book> books = query.list();

		return books;
	}

}
