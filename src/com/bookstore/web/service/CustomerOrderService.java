package com.bookstore.web.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.interfaces.ICustomerOrderDao;
import com.bookstore.web.dao.interfaces.IDiscountDao;
import com.bookstore.web.entities.Book;
import com.bookstore.web.entities.BookOrder;
import com.bookstore.web.entities.CustomerOrder;
import com.bookstore.web.entities.Discount;
import com.bookstore.web.entities.OrderItem;
import com.bookstore.web.entities.ShoppingCart;
import com.bookstore.web.entities.User;

@Service("customerOrderService")
public class CustomerOrderService {
	
	@Autowired
	private ICustomerOrderDao customerOrderDao;
	
	@Autowired
	private BookService bookService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private IDiscountDao discountDao;
	
	public CustomerOrder createNewOrder(ShoppingCart cart, String username) {
		
		User user = userService.findUserByUsername(username);
		
		CustomerOrder newOrder = new CustomerOrder();
		newOrder.setOrderDate(new Date());
		newOrder.setUser(user);
		
		customerOrderDao.persist(newOrder);		
		
		newOrder = findUsersLatestOrder(user);
		
		double totalItemPrice = cart.getTotalPrice();
		double paymentCharge = cart.getPaymentCharge();
		double totalPrice = totalItemPrice + paymentCharge;
		
		for(BookOrder order : cart.getBookOrders()) {
			OrderItem item = new OrderItem();
			order.setQuantity(1);
			
			Book book = bookService.findBookById(order.getBookId());
			
			totalPrice = totalPrice + book.getPrice();
			
			book.setStockLevel(book.getStockLevel() - order.getQuantity());
			bookService.merge(book);
			
			item.setBook(book);
			item.setOrder(newOrder);
			item.setQuantity(order.getQuantity());
			
			customerOrderDao.persist(item);
		}
		

		newOrder.setTotalPriceOfItems(totalItemPrice);
		newOrder.setPaymentCharge(paymentCharge);
		newOrder.setTotalPrice(totalPrice);
		
		System.out.println("USER's first purchase " + user.isFirstPurchase());
		 
		if(user.isFirstPurchase()) {
			
			Discount discount = new Discount();
			System.out.println("APPLYING DISCOUNT");
			newOrder.setDiscount(discount);
			
			discount.setCustomerOrder(newOrder);			
			newOrder.applyDiscount(discount);

			discountDao.persist(newOrder.getDiscount());
			
			user.setFirstPurchase(false);
			userService.merge(user);
			
		}
		
		customerOrderDao.merge(newOrder);

		newOrder = findUsersLatestOrder(user);
		return newOrder;
	}

	private CustomerOrder findUsersLatestOrder(User user) {
		
		CustomerOrder order = customerOrderDao.findUsersLatestOrder(user);
		
		List<OrderItem> orderItems = customerOrderDao.findOrderItemsByOrderId(order);
		
		order.setOrderItems(orderItems);
		
		for(OrderItem item : order.getOrderItems()) {
			item.setOrder(order);
		}
		
		return order;
	}

	public List<CustomerOrder> getAll(String username) {
		User user = userService.findUserByUsername(username);
		
		List<CustomerOrder> orders = getAllUsersOrders(user);
		
		return orders;
	}

	private List<CustomerOrder> getAllUsersOrders(User user) {

		List<CustomerOrder> orders = customerOrderDao.findAllUsersOrders(user);
	
		for(CustomerOrder order : orders) {
			List<OrderItem> items = customerOrderDao.findOrderItemsByOrderId(order);
			System.out.println("NUMBER OF ITEMS IN ORDER " + items.size());
			order.setOrderItems(items);
		}
		
		return orders;
	}

	
	
}
