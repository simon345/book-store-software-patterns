package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.web.dao.interfaces.IUserDao;
import com.bookstore.web.entities.CustomerOrder;
import com.bookstore.web.entities.User;

@Service("usersService")
public class UserService {
	
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private CustomerOrderService customerOrderService;
	
	@Transactional
	public void create(User user) {
		userDao.persist(user);
	}
	
	public List<User> getAllUsers() {
		return userDao.getAll();
	}

	public User findUserByUsername(String username) {
		return userDao.findByUsername(username);
	}

	public void merge(User user) {
		userDao.merge(user);
		
	}

	public User findUserById(int userId) {
		
		User user = userDao.findById(userId);
		List<CustomerOrder> orders = customerOrderService.getAll(user.getUsername());
		user.setOrders(orders);
		
		return user;
	}

}
