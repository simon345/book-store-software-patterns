package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.AuthorDao;
import com.bookstore.web.dao.interfaces.IAuthorDao;
import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;

@Service("authorService")
public class AuthorService {

	@Autowired
	private IAuthorDao authorDao;

	public AuthorService() { }

	public void create(Author author) {
		authorDao.persist(author);
	}
	
	public Author getAuthorByName(String authorName) {
		return authorDao.getAuthorByName(authorName);
	}
	
	public Author getAuthorByBook(Book b) {
		return authorDao.getAuthorByBook(b);
	}
	
	public List<Author> getAllAuthors() {
		return authorDao.getAll();
	}

	public Author findAuthorById(int id) {
		return authorDao.findById(id);
	}


}
