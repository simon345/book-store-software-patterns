package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookstore.web.dao.BookDao;
import com.bookstore.web.dao.interfaces.IBookDao;
import com.bookstore.web.dao.interfaces.ObjectDao;
import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;

@Service("bookService")
public class BookService {

	@Autowired
	private IBookDao bookDao;
	
	private AuthorService authorService;
	
	public List<Book> getAllBooks() {
		
		List<Book> books = bookDao.getAll();
		return books;
 	}
	
	public void create(Book book, String authorName) {
		
		Author author = authorService.getAuthorByName(authorName);
		book.setAuthor(author);
		
		bookDao.persist(book);
		
	}


	public Book findBookById(int bookId) {
		
		Book book = bookDao.findById(bookId);
		if(book != null) {
			Author author = authorService.findAuthorById(book.getAuthor().getId());
			book.setAuthor(author);
		}
		return book;
	}

	public void merge(Book book) {
		bookDao.merge(book);
	}
	
	@Autowired
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}
}
