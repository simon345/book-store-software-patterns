package com.bookstore.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bookstore.web.dao.AuthorDao;
import com.bookstore.web.dao.BookDao;
import com.bookstore.web.dao.interfaces.IAuthorDao;
import com.bookstore.web.dao.interfaces.IBookDao;
import com.bookstore.web.dao.interfaces.ObjectDao;
import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;

@Component("searchService")
public class SearchCommandService {

	@Autowired
	private IBookDao bookDao;
	
	@Autowired
	private IAuthorDao authorDao;
	
	public List<Book> searchForBooks(String searchTerm, String searchSelection) {
		
		if(searchSelection.equalsIgnoreCase("title")) {
			
			return bookDao.findBySearchTerm(searchTerm);
			
		} else if (searchSelection.equalsIgnoreCase("author")) {
			
			List<Author> authors = authorDao.findBySearchTerm(searchTerm);
			
			if(authors == null) {
				return null;
			}
			
			List<Book> books = bookDao.findBooksByAuthors(authors);
			
			return books;
		}
		
		return null;
	}
}
