package com.bookstore.web.controllers;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;
import com.bookstore.web.service.AuthorService;
import com.bookstore.web.service.BookService;
import com.bookstore.web.service.SearchCommandService;

@Controller
public class BookController {

	private BookService bookService;
	private SearchCommandService searchService;
	private AuthorService authorService;

	@RequestMapping(value = "/api/book/{bookId}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Book getBook(@PathVariable String bookId, Principal principal) {

		System.out.println("GETTING BOOK");
		System.out.println(bookId);

		int id = Integer.parseInt(bookId);

		Book book = bookService.findBookById(id);
		System.out.println("BOOK _ " + book.getName());
		System.out.println(book.getAuthor().getName());
		System.out.println("ENDKLKLKSJKDSJLKA");

		return book;
	}

	@RequestMapping(value = "/book/edit/{bookId}")
	public String editBook(@PathVariable("bookId") Integer bookId, Model model) {
		
		
		Book book = bookService.findBookById(bookId);
		model.addAttribute("book", book);

		return "editbook";
	}
	
	@RequestMapping(value = "/doeditbook", method = RequestMethod.POST)
	public String doEditBook(@RequestParam("author") int authorId, Model model, Book book, BindingResult result) {

		System.out.println("BOOK " + book.getName() + " " + book.getId() + " " + book.getStockLevel() + " author " + authorId);
		
		Author author = authorService.findAuthorById(authorId);
		book.setAuthor(author);
		
		bookService.merge(book);
	
		book = bookService.findBookById(book.getId());
		model.addAttribute("book", book);
		
		return "book";
	}

	@RequestMapping(value = "/books/{bookId}")
	public String showBook(@PathVariable("bookId") Integer bookId, Model model) {

		System.out.println("Book id" + bookId);
		Book book = bookService.findBookById(bookId);
		model.addAttribute("book", book);
		System.out.println("BOOK _ " + book.getName());
		System.out.println(book.getAuthor().getName());

		return "book";
	}

	@RequestMapping("/books")
	public String showBooks(Model model) {

		List<Book> books = bookService.getAllBooks();
		model.addAttribute("books", books);
		return "books";

	}

	@RequestMapping(value = "/dosearch", method = RequestMethod.POST)
	public String searchBooks(@RequestParam("name") String searchTerm,
			@RequestParam("searchSelection") String searchSelection, Model model) {

		List<Book> books = searchService.searchForBooks(searchTerm, searchSelection);
		model.addAttribute("books", books);

		return "books";

	}

	@RequestMapping(value = "/docreatebook", method = RequestMethod.POST)
	public String doCreate(@RequestParam("author") String authorName, Model model, Book book, BindingResult result) {

		bookService.create(book, authorName);

		List<Book> books = bookService.getAllBooks();
		model.addAttribute("books", books);
		return "books";
	}

	@RequestMapping("/createbook")
	public String createBook(Model model) {

		List<Author> authors = authorService.getAllAuthors();
		model.addAttribute("authors", authors);

		model.addAttribute("book", new Book());
		return "createbook";
	}

	@Autowired
	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	@Autowired
	public void setSearchService(SearchCommandService searchService) {
		this.searchService = searchService;
	}

	@Autowired
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

}
