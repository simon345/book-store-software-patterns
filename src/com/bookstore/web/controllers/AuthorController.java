package com.bookstore.web.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bookstore.web.entities.Author;
import com.bookstore.web.entities.Book;
import com.bookstore.web.service.AuthorService;

@Controller
public class AuthorController {

	private AuthorService authorService;

	@RequestMapping("/authors")
	public String showAuthors(Model model) {
		List<Author> authors = authorService.getAllAuthors();
		model.addAttribute("authors", authors);
		return "authors";
	}
	
	@RequestMapping("/createauthor")
	public String createOffer(Model model) {

		model.addAttribute("author", new Author());
		return "createauthor";
	}
	
	@RequestMapping("/authors/{authorId}")
	public String showAuthor(@PathVariable("authorId") Integer id, Model model, Principal principal) {
		System.out.println("ID " + id);
		
		Author author = authorService.findAuthorById(id);
		model.addAttribute("author", author);
		
		return "author";
	}

	@RequestMapping(value = "/docreateauthor", method = RequestMethod.POST)
	public String doCreate(Model model, Author author, BindingResult result) {

		authorService.create(author);

		List<Author> authors = authorService.getAllAuthors();
		model.addAttribute("authors", authors);

		return "authors";
	}

	@Autowired
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

}
