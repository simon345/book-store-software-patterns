package com.bookstore.web.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.entities.BookOrder;
import com.bookstore.web.entities.CustomerOrder;
import com.bookstore.web.entities.ShoppingCart;
import com.bookstore.web.entities.User;
import com.bookstore.web.service.CustomerOrderService;
import com.bookstore.web.service.UserService;

@Controller
public class OrderController {

	@Autowired	
	private CustomerOrderService customerOrderService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/checkout")
	public String showCheckout(Principal principal, Model model) {
		
		String username = principal.getName();
		User user = userService.findUserByUsername(username);
		
		model.addAttribute("user", user);
		
		return "checkout";
	}

	@RequestMapping("/orders")
	public String showOrders(Principal principal, Model model) {
		
		String username = principal.getName();
		User user = userService.findUserByUsername(username);
		List<CustomerOrder> orders = customerOrderService.getAll(username);
		System.out.println("NUM ORDERS RETURNED " + orders.size());
		model.addAttribute("orders", orders);
		model.addAttribute("user", user);
		return "orders";
	}

	@RequestMapping(value = "/api/order/create", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public CustomerOrder createOrder(@RequestBody ShoppingCart cart, Principal principal, Model model) {

		System.out.println("CREATE ORDER");
		System.out.println(cart.getBookOrders().size());
		
		String username = principal.getName();
		
		CustomerOrder order = customerOrderService.createNewOrder(cart, username);
		
		return order;

	}

}
