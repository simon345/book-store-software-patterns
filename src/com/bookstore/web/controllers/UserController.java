package com.bookstore.web.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bookstore.web.entities.User;
import com.bookstore.web.service.BookService;
import com.bookstore.web.service.UserService;

@Controller
public class UserController {

	private UserService userService;
	
	@RequestMapping(value = "/api/user/current", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public User currentUserDetails(Principal principal) {
		System.out.println("GETTING USER");
		String username = principal.getName();
		User user =  userService.findUserByUsername(username);
	    System.out.println("GETTING USER " + user.getName());
		return user;
	}
	
	@RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
	public String showUser(@PathVariable Integer userId,Principal principal, Model model) {
		
		User user = userService.findUserById(userId);
		model.addAttribute("user", user);
		
		System.out.println("NUMBER OF ORDERS RETURNED " + user.getOrders().size());
		
		return "user";
		
	}
	
	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}

	@RequestMapping("/newaccount")
	public String showNewAccount(Model model) {

		model.addAttribute("user", new User());
		return "newaccount";
	}
	
	@RequestMapping(value = "/createaccount", method = RequestMethod.POST)
	public String createAccount(User user, Model model) {

		user.setAuthority("ROLE_USER");
		user.setEnabled(true);
		user.setFirstPurchase(true);
		
		userService.create(user);

		return "login";
	}
	
	@RequestMapping("/admin")
	public String showAdmin(Model model) {

		List<User> users = userService.getAllUsers();
		model.addAttribute("users", users);
		return "admin";
	}
	
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
